package main

import (
	"database/sql"
	"log"
	_ "github.com/go-sql-driver/mysql"
	"fmt"
	"math/rand"
	"os"
	"strings"
	"time"
)

const str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func RandomString(size int) string {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	var stock []string
	for x := 0; x < size; x ++ {
		index := r.Intn(len(str))
		stock = append(stock, string(str[index]))
	}
	return strings.Join(stock, "")
}

type User struct {
	Name   string
	Family string
	Age    int
}

func main() {
	for i := 1; i < 10; i++ {
		fmt.Printf("[%3d] `%v` \n", i, RandomString(i))
	}

	os.Exit(1)

	//connect database
	db, err := sql.Open("mysql", "root:spouknet@tcp(:3306)/best")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	//create table
	_, err = db.Exec("create table IF NOT  EXISTS  best.hello(world varchar(50))")
	if err != nil {
		log.Fatal(err.Error())
	}
	db.Exec()


	//insert records

}
