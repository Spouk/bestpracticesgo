package main

import (
	"flag"
	"fmt"
	ttt "gitlab.com/bestpracticesgo/servers/tcp"
	"log"
	"os"
)

var (
	port string
)

func init() {
	flag.StringVar(&port, "port", "", "tcp port for bind server for listening")
}

func main() {
	//parse incoming flags
	flag.Parse()

	//check if not found flags
	//if len(os.Args) < 2 {
	//
	//}
	if port == "" {
		fmt.Printf(fmt.Errorf("Error user help for print defaults flags").Error())
		os.Exit(-1)
	}

	s := ttt.NewServerExample(port)
	fmt.Printf("Server instance: %v\n", s)
	log.Fatal(s.Run())

}
