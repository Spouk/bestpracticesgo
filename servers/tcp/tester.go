package tcp

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
	"os"
)

type ServerExample struct {
	L       *log.Logger
	counter int64
	port    string
}

//make new instance
func NewServerExample(port string) *ServerExample {
	n := &ServerExample{
		L: log.New(os.Stdout, "[tcp-server-example]", log.Lshortfile|log.Ldate|log.Ltime),
	}
	n.port = fmt.Sprintf(":%s",port)
	return n
}

//run tcp server for listening port
func (s ServerExample) Run() error {
	//create listener
	listener, err := net.Listen("tcp", s.port)
	if err != nil {
		s.L.Printf("Error making listener : %v",err.Error())
		return err
	}

	//loop for get incoming connections
	for {
		conn, err := listener.Accept()
		if err != nil {
			s.L.Printf(err.Error())
			continue
		}
		//accept connect and stich to goroutine
		go s.worker(conn, fmt.Sprintf("#_%d", s.counter+1))

		//add counter connections
		s.counter++
	}
}
func (s ServerExample) worker(conn net.Conn, prefix string) {
	defer func() {
		s.counter--
	}()
	s.L.Printf(fmt.Sprintf("worker [%s] starting...", prefix))
	reader := bufio.NewReader(conn)
	for {
		//read client data request
		bytes, err := reader.ReadBytes(byte('\n'))
		if err != nil {
			if err != io.EOF {
				s.L.Printf(fmt.Sprintf("worker [%s] [error] %s\n", prefix, err.Error()))
			}
			return
		}
		fmt.Printf("request: %s\n", bytes)

		//make response
		resp := fmt.Sprintf("%s %s", prefix, bytes)
		count, err := conn.Write([]byte(resp))
		if err != nil {
			s.L.Printf(fmt.Sprintf("worker (%s) write response [error] %v\n", prefix, err.Error()))
			continue
		}
		s.L.Printf(fmt.Sprintf("[success] worker (%s) out count %d bytes\n", count))
	}
}
