package main

import (
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"fmt"
	"github.com/labstack/gommon/log"
	"os"
)

var (
	v = viper.New()
)

type conf struct {
	Prod struct {
		Host    string
		Port    string
		Enabled bool
	}
	Dev struct {
		Host    string
		Port    string
		Enabled bool
	}
	Qa struct {
		Host    string
		Port    string
		Enabled bool
	}
}
type prod struct {
	Host    string
	Port    string
	Enabled bool
}

func main() {
	//set config file for netx step read this
	v.SetConfigFile("/stock/s.develop/go/src/gitlab.com/bestpracticesgo/configuration/env.json")
	v.SetConfigType("json")

	//read config file
	if err := v.ReadInConfig(); err != nil {
		fmt.Printf(err.Error())
		return
	}
	fmt.Printf("Reading config file: %v\n", v.ConfigFileUsed())
	res := v.Get("dev")
	fmt.Printf("Some values: %v  : %v\n", res, v.Get("dev.host"))

	//read in struct config file
	var p conf
	//prod := v.Sub("prod")
	err := v.Unmarshal(&p)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Result %v\n", p)


	//flag example
	var cc  = pflag.String("test", "", "testing config filename")
	pflag.Parse()
	pflag.Usage = func () {
		fmt.Printf("USAGE: %v\n", os.Args[0])
	}
	pflag.Usage()

	if *cc != ""  {
		fmt.Printf("Result parser: %v\n", *cc)
	} else {
		pflag.PrintDefaults()
	}



}
//---------------------------------------------------------------------------
//  config params
//- configwork
//- configdev
//---------------------------------------------------------------------------
